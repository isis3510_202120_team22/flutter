import 'package:condominium/login/login.dart';
import 'package:condominium/welcome/welcome.dart';
import 'package:flutter/material.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text("Condominium",
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 45)),
          SizedBox(
            height: size.height * 0.15,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: TextButton(
                style: TextButton.styleFrom(
                    backgroundColor: kButtonColor,
                    primary: kPrimaryColor,
                    padding:
                        EdgeInsets.symmetric(vertical: 12, horizontal: 80)),
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return LoginScreen();
                      },
                    ),
                  );
                },
                child: Text("Login", style: TextStyle(fontSize: 18))),
          ),
          SizedBox(
            height: size.height * 0.02,
          ),
          ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: TextButton(
                style: TextButton.styleFrom(
                    backgroundColor: kLigthPrimary,
                    primary: kPrimaryColor,
                    padding:
                        EdgeInsets.symmetric(vertical: 12, horizontal: 20)),
                onPressed: () {},
                child: Text("Register your condo",
                    style: TextStyle(fontSize: 18))),
          ),
        ],
      ),
    );
  }
}
