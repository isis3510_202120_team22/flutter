import 'package:condominium/welcome/components/body.dart';
import 'package:flutter/material.dart';

//AZUL OSCURO
const kPrimaryColor = Color(0xFF1A237E);

const kLigthPrimary = Color(0xFF2962FF);

//AZUL CLARO
const kButtonColor = Color(0xFF82B1FF);

//FUNTE
class MyHomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Body(),
      backgroundColor: kPrimaryColor,
    );
  }
}
