import 'package:condominium/home/home.dart';
import 'package:condominium/login/login.dart';
import 'package:condominium/payments/payments_view.dart';
import 'package:condominium/visitors/add_visitor_view.dart';
import 'package:condominium/visitors/visitors_view.dart';
import 'package:condominium/welcome/welcome.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  var token = prefs.getString('token');
  print("el token es: " + token.toString());

  runApp(MaterialApp(                                                                        
      title: 'Flutter Demo',
      theme: ThemeData(
        backgroundColor: const Color(0xFF22223B),
        cardColor: const Color(0xFF92A3FD),
        highlightColor: const Color(0xFF4A4E69),


        textTheme: const TextTheme(
          headline1: TextStyle(
            fontSize: 24.0,
            fontWeight: FontWeight.bold,
            fontFamily: "Poppins",
            color: Colors.white,
          ),
          headline2: TextStyle(
            fontSize: 12.0,
            color: Color(0xFFFFFFFF)
          ),
          headline3: TextStyle(
            fontSize: 12.0,
            color: Color(0xFF000000)
          ),
          subtitle2: TextStyle(
            fontSize: 10.0
          ),
        ),
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: token == null ? MyHomePage(): HomeScreen()
      //home: HomeScreen(),
    ));
  
}
