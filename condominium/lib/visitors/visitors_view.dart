import 'package:condominium/models/visitors/visitor.dart';
import 'package:condominium/utils/eventual_connectivity.dart';
import 'package:condominium/visitors/add_visitor_view.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:condominium/utils/widgets.dart';
import 'package:condominium/visitors/visitors_view_model.dart';


class Visitors extends StatefulWidget {
  const Visitors({Key? key}) : super(key: key);

  @override
  VisitorsScreen createState() => VisitorsScreen();
}

class VisitorsScreen extends State<Visitors>{

  late Future<List<Visitor>> _visitorsList;
  late Future<List<Visitor>> _frequentVisitorsList;
  late Future<List<Visitor>> _visitorsCacheList;
  late Future<List<Visitor>> _frequentVisitorsCacheList;
  ConnectivityResult _online = ConnectivityResult.none;

  final VisitorsViewModel _visitorsViewModel = VisitorsViewModel();

  @override
  void initState() {
    super.initState();
    _visitorsList = _visitorsViewModel.getVisitors();
    _frequentVisitorsList = _visitorsViewModel.getFrequentVisitors();
    _visitorsCacheList = _visitorsViewModel.getVisitorsFromCache();
    _frequentVisitorsCacheList =_visitorsViewModel.getFrequentVisitorsFromCache();
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _online = result;
    });
  }

  @override
  Widget build(BuildContext context) {
     return Scaffold(
       backgroundColor: Theme.of(context).backgroundColor,
       body: SafeArea(
         child: Column(
           children: <Widget>[
             tittle(context),
             EventualConnectivity(updateConnectionStatus: _updateConnectionStatus),
             _areNoFrequentVisitors(),
             _frequentVisitors(),
             _visitors()
           ],
         ),
       ),
      floatingActionButton: addButton(context),
      bottomNavigationBar: bottomBar(context),
     );
  }

  Widget addButton(context){
    return FloatingActionButton(
      backgroundColor: _online != ConnectivityResult.none? Theme.of(context).cardColor : Colors.grey,
      child: const Icon(Icons.add),
      onPressed: () {
        if(_online != ConnectivityResult.none) Navigator.push(context, MaterialPageRoute(builder: (context) => const AddVisitorScreen()));
      }
    );
  }

  Widget _frequentVisitors(){
    return Column( 
      children: <Widget> [
        Padding(
          padding: const EdgeInsets.only(left: 30.0, right: 30.0, top: 5.0, bottom: 15.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                "Frequent Visitors",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                )),
            ],
          ),
        ),
        SizedBox(
          height: 200,
          child: _asyncList(true),
        )

        ]
    );
 
  }

  Widget _areNoFrequentVisitors(){
    return FutureBuilder(
      future: _visitorsViewModel.getNonFrequentVisitors(),
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return Container(
            padding: const EdgeInsets.all(10.0),
            child: _noFrequentVisitors(snapshot.data , context)
          );
        } else if (snapshot.hasError) {
          return  Container(
            padding: const EdgeInsets.all(15.0),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    );
  }

  Widget _noFrequentVisitors(visitors, context){
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Container(
        decoration: _decoration(),
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: const <Widget>[
            Icon(Icons.notification_add),
            SizedBox(width: 10),
            Text("You have visitors that have \n not come in the last 3 months"),
            
          ],
        ),
      ),
    );
  }

  BoxDecoration _decoration(){
    return BoxDecoration(
      color: const Color(0xFFC58BF2),
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(4.0, 4.0), 
          blurRadius: 10.0
        ),
      ]
    );
  }

  Widget _visitors(){
    return Column( 
      children: <Widget> [
        Padding(
          padding: const EdgeInsets.only(left: 30.0, right: 30.0, top: 5.0, bottom: 15.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                "Visitors",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20
                )),
            ],
          ),
        ),
        SizedBox(
          height: 200,
          child: _asyncList(false),
        )

        ]
    );
 
  }


  Widget _asyncList(bool frequents) {
    return FutureBuilder(
      future: frequents? _frequentVisitorsCacheList : _visitorsCacheList,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return _asyncCacheList(snapshot, frequents);
        } else if (snapshot.hasError) {
          return const Center(
            child: Icon(Icons.error),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    );
  }

  Widget _asyncCacheList(AsyncSnapshot snapshotCache, bool frequents) {
    return FutureBuilder(
      future: frequents? _frequentVisitorsList : _visitorsList,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          frequents? _visitorsViewModel.saveFrequentVisitors(snapshot.data as List<Visitor>) :
            _visitorsViewModel.saveVisitors(snapshot.data as List<Visitor>);
          return ListView(
            padding: const EdgeInsets.all(10.0),
            children: _visitorsCards(snapshot.data as List<Visitor>),
          );
        } else if (snapshot.hasError) {
          return ListView(
            padding: const EdgeInsets.all(10.0),
            children: _visitorsCards(snapshotCache.data as List<Visitor>),
          );
        } else {
          return ListView(
            padding: const EdgeInsets.all(10.0),
            children: _visitorsCards(snapshotCache.data as List<Visitor>),
          );
        }
      }
    );
  }


  List<Widget> _visitorsCards(List<Visitor> visitors) {
    if (visitors.isEmpty) {
      return List<Widget>.filled(1,
        Text(
          "You have no visitors",
          //style: Theme.of(context).textTheme.headline2
        ));
    }


    List<Widget> paymentsWidgets = [];

    for (var payment in visitors) {
      paymentsWidgets.add(
        Padding(
          padding: const EdgeInsets.only(bottom: 15),
          child: Container(
            height: 70,
            decoration: _boxDecoration(context),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Image.asset('assets/img/visitante.jpeg'),
                Text(payment.firstName+" "+payment.lastName),

              ],
            )),
          )
        
        
      );
      
    }

    return paymentsWidgets;
  }

  BoxDecoration _boxDecoration(context) {
    return BoxDecoration(
      color: Theme.of(context).cardColor,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }

  Widget _userLogo() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Image.asset('assets/img/visitante.png'),
    );
  }

}