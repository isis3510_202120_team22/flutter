import 'package:flutter/material.dart';
import 'package:condominium/utils/widgets.dart';

class AddVisitorScreen extends StatelessWidget {
  const AddVisitorScreen({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
     return Scaffold(
       backgroundColor: Theme.of(context).backgroundColor,
       body: SafeArea(
         child: Column(
           children: <Widget>[
             tittle(context),
             _userLogo(),
             _addForm(context),
             addButton()
           ],
         ),
       ),
      bottomNavigationBar: bottomBar(context),
     );
  }

  Widget _userLogo() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Image.asset('assets/img/visitante.jpeg'),
    );
  }

  Widget _addForm(context) {
    return Form(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25, bottom: 15),
            child: Container(
              decoration: _boxDecoration(context),
              child: TextFormField(
              decoration: const InputDecoration(
                icon: Icon(Icons.person),
                labelText: "First Name"
              ),
            
          )
          ),),

          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25, bottom: 15),
            child: Container(
              decoration: _boxDecoration(context),
              child: TextFormField(
              decoration: const InputDecoration(
                icon: Icon(Icons.badge),
                labelText: "Last Name"
              ),
            
          )
          ),),
          
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25, bottom: 15),
            child: Container(
              decoration: _boxDecoration(context),
              child: TextFormField(
              decoration: const InputDecoration(
                icon: Icon(Icons.account_balance_wallet),
                labelText: "Id Type"
              ),
            
          )
          ),),
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25, bottom: 15),
            child: Container(
              decoration: _boxDecoration(context),
              child: TextFormField(
              decoration: const InputDecoration(
                icon: Icon(Icons.dialpad),
                labelText: "Id Number"
              ),
            
          )
          ),),
          Padding(
            padding: const EdgeInsets.only(left: 25, right: 25, bottom: 15),
            child: Container(
              decoration: _boxDecoration(context),
              child: TextFormField(
              decoration: const InputDecoration(
                icon: Icon(Icons.phone),
                labelText: "Telephone"
              ),
            
          )
          ),)          
          
        ])
        );

  }

  Widget addButton(){
    return ElevatedButton(
      style:  ButtonStyle(
        backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF92A3FD))
      ),
      onPressed: (){

      }, 
      child: const Text("Add visitor")
      );
  }

  BoxDecoration _boxDecoration(context) {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }

}