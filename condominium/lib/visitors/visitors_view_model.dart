import 'package:condominium/analytics/analytics.dart';
import 'package:condominium/models/cache/condo_cache_manager.dart';
import 'package:condominium/models/user/user_singleton.dart';
import 'package:condominium/models/visitors/visitor.dart';
import 'package:condominium/models/visitors/visitor_model.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class VisitorsViewModel {

  User user = UserSingleton().currentUser;

  VisitorModel model =VisitorModel();

  final Analytics _analytics = Analytics();

  Future<List<Visitor>> getVisitors(){
    return model.getVisitors(user.token);
  }

  Future<List<Visitor>> getNonFrequentVisitors(){
    return model.getNonFrequentVisitors(user.token);
  }

  Future<List<Visitor>> getVisitorsFromCache() {
    return model.getVisitorsFromCache(user.token);
  }

  Future<void> saveVisitors(List<Visitor> visitors) async {
    model.saveVisitors(user.token, visitors);
  }

  Future<List<Visitor>> getFrequentVisitors(){
    return model.getFrequentVisitors(user.token);
  }
  
  Future<List<Visitor>> getFrequentVisitorsFromCache() {
    return model.getFrequentVisitorsFromCache(user.token);
  }

  Future<void> saveFrequentVisitors(List<Visitor> visitors) async {
    model.saveFrequentVisitors(user.token, visitors);
  }

  void informAccess(ConnectivityResult online) {
    bool isOnline = !(online == ConnectivityResult.none);
    _analytics.onlineTrace(user.token, isOnline, ViewType.visitors);
  }
}