import 'dart:async';
import 'dart:io';

import 'package:condominium/home/home.dart';
import 'package:condominium/models/user/user_singleton.dart';
import 'package:condominium/welcome/welcome.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:crypto/crypto.dart';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';


class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {


bool logged = false;

  void checkBoxCallBack(bool checkBoxState) {
    setState(() {
      logged = checkBoxState;
    });
  }

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();



static Route<Object?> _dialogBuilder(BuildContext context, Object? arguments) {
  return DialogRoute<void>(
    context: context,
    builder: (BuildContext context) => const AlertDialog(title: Text('Material Alert!')),
  );
}

  Future<void> login() async {
    //inicio de contador
    var inicio = DateTime.now().millisecondsSinceEpoch;

    String user = emailController.text;
    String pass = passwordController.text;
    
    print(pass);
    var bytes = utf8.encode(pass);
    Digest md5Result = md5.convert(bytes);

    print("este es el HASH: " + md5Result.toString());

    var url = 'https://fathomless-everglades-41121.herokuapp.com/users/' + user;

    print(url);

    Map input = {"inserted_pwd_hash": "" + '$md5Result'};
    var body = json.encode(input);

    print(body);

    var response;

    http.Response bq1_http_request;

    try{
      response = await http.post(Uri.parse(url),
                               headers: {"Content-Type": "application/json"},
                               body: body);
    //var tiempoTotal = DateTime.now().millisecondsSinceEpoch - inicio;
    print(response.statusCode);

    if (response.statusCode == 200) {
      String body = utf8.decode(response.bodyBytes);
      final jsonData = jsonDecode(body);

      UserSingleton().setUser(new User(jsonData["token"]));

       print("token saved in singleton " + UserSingleton().currentUser.token);

        

        String bq1_url = 'https://fathomless-everglades-41121.herokuapp.com/notifications/' 
                            + UserSingleton().currentUser.token;
                            
                            
        if(Platform.isIOS){
          bq1_url += '/ios/';
        }
        else if(Platform.isAndroid){
          bq1_url += '/android/';
        }

        // SI SELECCIONO RECORDARME GUARDAR TOKEN EN SHARED PREFERENCES
        if(this.logged){

          bq1_url += 'rememberyes';      
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('token', UserSingleton().currentUser.token);        
          
        }else{
          bq1_url += 'rememberno';
        }

        
        bq1_http_request = await http.get(Uri.parse(bq1_url));
        print("" + bq1_url);
        print("registro bq1 enviado, http get status code: " + bq1_http_request.statusCode.toString());
                




      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) {
            return HomeScreen();
          },
        ),
      );
    } else if(response.statusCode == 404 || response.statusCode ==401) {
      //hubo un errorcon las credenciales
      
      Navigator.push(context, MaterialPageRoute(builder: (context) {
        return AlertDialog(
          title: new Text("Authentication error"),
          content: Text("check your credentials"),
        
          actions: <Widget>[
            new TextButton(
              child: new Text("Back"),
              onPressed: () {
                //Navigator.pushNamed(context, "/screen1");
                Navigator.pop(context);
                
              },
            ),
          ],
        );
      }));
      
    }

    } catch (e) {
    if(e is SocketException){
       //treat SocketException
       print("Socket exception: ${e.toString()}");
       Navigator.push(context, MaterialPageRoute(builder: (context) {
        return AlertDialog(
          title: new Text("connection failed"),
          content: Text("there was an error triying to verify your information.\nCheck your internet conection"),
        
          actions: <Widget>[
            new TextButton(
              child: new Text("Back"),
              onPressed: () {
                //Navigator.pushNamed(context, "/screen1");
                Navigator.pop(context);
                
              },
            ),
          ],
        );
      }));
    }
    else if(e is TimeoutException){
       //treat TimeoutException
       print("Timeout exception: ${e.toString()}");
       Navigator.push(context, MaterialPageRoute(builder: (context) {
        return AlertDialog(
          title: new Text("Unable to connect"),
          content: Text("the timeout to reach the servers was reached"),
        
          actions: <Widget>[
            new TextButton(
              child: new Text("Back"),
              onPressed: () {
                //Navigator.pushNamed(context, "/screen1");
                Navigator.pop(context);
                
              },
            ),
          ],
        );
      }));
    }
    else print("Unhandled exception: ${e.toString()}");
  }
    

    

     

  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: kPrimaryColor,
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Condominium",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 45)),
            SizedBox(
              height: size.height * 0.02,
            ),
            Text("Hey there",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 15)),
            Text("Welcome Back",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 25)),
            SizedBox(
              height: size.height * 0.15,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
              width: size.width * 0.75,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: TextField(               
                controller: emailController,
                decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    hintText: 'Username',
                    border: InputBorder.none),
              ),
            ),
            SizedBox(
              height: size.height * 0.02,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
              width: size.width * 0.75,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(10)),
              child: TextField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  icon: Icon(Icons.lock),
                  hintText: 'Password',
                  border: InputBorder.none,
                  suffixIcon: Icon(Icons.visibility),
                ),
              ),
            ),
            SizedBox(
              height: size.height * 0.02,
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  width: 10,
                ), //SizedBox
                Text(
                  'Remember me ',
                  style: TextStyle(fontSize: 17.0, color: Colors.white),
                ), //Text
                SizedBox(width: 10), //SizedBox
                /** Checkbox Widget **/
                Checkbox(
                  value: this.logged,
                  onChanged: (newValue) {
                    setState(() {
                      if (newValue == true) {
                        this.logged = true;
                      } else {
                        this.logged = false;
                      }
                      print("nuevo valor RECORDARME:" + logged.toString());
                    });
                  },
                ) //Checkbox
              ], //<Widget>[]
            ),




            ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: TextButton(
                  style: TextButton.styleFrom(
                      backgroundColor: kButtonColor,
                      primary: kPrimaryColor,
                      padding:
                          EdgeInsets.symmetric(vertical: 12, horizontal: 80)),
                  onPressed: () {
                    login();
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) {
                    //       return HomeScreen();
                    //     },
                    //   ),
                    // );
                  },
                  child: Text("Login", style: TextStyle(fontSize: 18))),
            ),
            SizedBox(
              height: size.height * 0.02,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Having problems?", style: TextStyle(color: Colors.white)),
                GestureDetector(
                    onTap: () {},
                    child: Text(
                      " Reset your password",
                      style: TextStyle(
                          color: kLigthPrimary, fontWeight: FontWeight.bold),
                    ))
              ],
            )
          ],
        ),
      ),
    );
  }
}


