import 'package:condominium/analytics/analytics.dart';
import 'package:condominium/models/cache/condo_cache_manager.dart';
import 'package:condominium/models/payments/payment.dart';
import 'package:condominium/models/payments/payment_model.dart';
import 'package:condominium/models/user/user_singleton.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class PaymentsViewModel {

  User user = UserSingleton().currentUser;

  PaymentModel model = PaymentModel();

  Analytics analytics = Analytics();

  Future<List<Payment>> getPayments() {
    return model.getPayments(user.token);
  }

  Future<bool> getNearPayments(){
    return model.getNextPayments(user.token);
  }

  Future<double> getMonthPaid() {
    return model.getMonthPaid(user.token);
  }

  Future<List<Payment>> getPaymentsFromCache() {
    return model.getPaymentsFromCache(user.token);
  }

  Future<void> savePayments(List<Payment> payments) async {
    List<Payment> toSave = [];
    for (var i = 0; toSave.length != 10 && i < payments.length; i++) {
      if (payments[i].status == PaymentStatus.pending) toSave.add(payments[i]);
    }
    model.savePayments(toSave);
    analytics.cachedData(user.token, toSave.length);
  }

  void informAccess(ConnectivityResult online) {
    bool isOnline = !(online == ConnectivityResult.none);
    analytics.onlineTrace(user.token, isOnline, ViewType.payments);
  }
}