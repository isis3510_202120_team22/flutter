
import 'dart:async';

import 'package:condominium/camera/camera.dart';
import 'package:condominium/models/payments/payment.dart';
import 'package:condominium/payments/payments_view_model.dart';
import 'package:condominium/utils/eventual_connectivity.dart';
import 'package:condominium/utils/widgets.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:flutter/services.dart';

class Payments extends StatefulWidget {
  const Payments({Key? key}) : super(key: key);

  @override
  _PaymentsState createState() => _PaymentsState();
}

class _PaymentsState extends State<Payments> {

  late Future<List<Payment>> _paymentsList;

  late Future<List<Payment>> _paymentsCacheList;

  ConnectivityResult _online = ConnectivityResult.none;

  late Future<double> _number;

  final PaymentsViewModel _paymentsViewModel = PaymentsViewModel();

  @override
  void initState() {
    super.initState();
    _paymentsList = _paymentsViewModel.getPayments();
    _paymentsCacheList = _paymentsViewModel.getPaymentsFromCache();
    _number = _paymentsViewModel.getMonthPaid();
  }

  void informAccess() {
    _paymentsViewModel.informAccess(_online);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    setState(() {
      _online = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: tittle(context),
            floating: true,
            backgroundColor: const Color(0xFF22223B),
          ),
          SliverFixedExtentList(
            itemExtent: 30.0,
            delegate: SliverChildListDelegate([
              EventualConnectivity(updateConnectionStatus: _updateConnectionStatus),
              _numberText()
            ])
          ),
          SliverFillRemaining(
            child: _asyncList()
          ),
        ],
      ),
      bottomNavigationBar: bottomBar(context),
    );
  }

  Widget _numberText() {
    if (_online == ConnectivityResult.none) return Container();
    return FutureBuilder(
      future: _number,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return Container(
            margin: const EdgeInsets.symmetric(horizontal:15, vertical: 5),
            decoration: _boxDecoration(context),
            alignment: Alignment.center,
            child: Text(
            "This month you will pay: \$${snapshot.data}",
            style: const TextStyle(color: Colors.black),
            ),
          );
          
        } else if (snapshot.hasError) {
          return const Center(
            child: Icon(Icons.error),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    );
  }

  Widget _asyncList() {
    return FutureBuilder(
      future: _paymentsCacheList,
      builder: (contextCache, snapshotCache) {
        if(snapshotCache.hasData) {
          return _asyncCacheList(snapshotCache);
        } else if (snapshotCache.hasError) {
          return const Center(
            child: Icon(Icons.error),
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    );
  }

  Widget _asyncCacheList(AsyncSnapshot snapshotCache) {
    return FutureBuilder(
      future: _paymentsList,
      builder:(context, snapshot) {
        if(snapshot.hasData){
          _paymentsViewModel.savePayments(snapshotCache.data as List<Payment>);
          return ListView(
            padding: const EdgeInsets.all(10.0),
            children: _paymentCards(snapshot.data as List<Payment>),
          );
        } else if (snapshot.hasError) {
          return ListView(
            padding: const EdgeInsets.all(10.0),
            children: _paymentCards(snapshotCache.data as List<Payment>),
          );
        } else {
          return ListView(
            padding: const EdgeInsets.all(10.0),
            children: _paymentCards(snapshotCache.data as List<Payment>),
          );
        }
      }
    );
  }

  List<Widget> _paymentCards(List<Payment> payments) {
    if (payments.isEmpty) {
      return List<Widget>.filled(1, 
        Text(
          "You have no pending payments",
          style: Theme.of(context).textTheme.headline2,
        )
      );
    }
    
    List<Widget> paymentsWidgets = [];

    for (var payment in payments) {
      paymentsWidgets.add(
         Padding(
          padding: const EdgeInsets.all(15),
          child: 
          Container(
            decoration: _boxDecoration(context),
            child: Column(
              children:  <Widget> [
                ListTile(
                  leading: payment.status == PaymentStatus.paid? Icon(Icons.check_circle) : _selectIcon(payment.dueDate),
                  title: Text(payment.concept),
                  subtitle: Text('Total: \$${payment.amount}\nDue date: ${payment.dueDate.day}-${payment.dueDate.month}-${payment.dueDate.year}'),
                  isThreeLine: true,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    TextButton(
                      child: const Text('Upload Paid invoice', style: TextStyle(color: Colors.white),),
                      onPressed: _online == ConnectivityResult.none? null : () async {

                        // Ensure that plugin services are initialized so that `availableCameras()`
                        // can be called before `runApp()`
                        WidgetsFlutterBinding.ensureInitialized();

                        // Obtain a list of the available cameras on the device.
                        final cameras = await availableCameras();

                        // Get a specific camera from the list of available cameras.
                        final firstCamera = cameras.first;
                        Navigator.push(context, MaterialPageRoute(builder: (context) => TakePictureScreen(camera: firstCamera)));
                      },
                      style: ButtonStyle(
                      backgroundColor:MaterialStateProperty.all<Color>(Theme.of(context).backgroundColor) ,
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Theme.of(context).backgroundColor)
                        )
                      )
                    ),
                    ),
                    const SizedBox(width: 8),
                    TextButton(
                      child: const Text('Pay', style: TextStyle(color: Colors.white),),
                      onPressed: _online == ConnectivityResult.none? null : () {/* ... */},
                      style: ButtonStyle(
                      backgroundColor:MaterialStateProperty.all<Color>(Theme.of(context).backgroundColor) ,
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Theme.of(context).backgroundColor)
                        )
                      )
                    ),
                    ),
                    const SizedBox(width: 8),
                  ],
                ),
              ],
            ),
          )
          
        
      ));
      
    }

    return paymentsWidgets;
  }

  BoxDecoration _boxDecoration(context) {
    return BoxDecoration(
      color: Theme.of(context).cardColor,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }

  Icon _selectIcon(DateTime dueDate) {
    DateTime now = DateTime.now();
    if (now.isBefore(dueDate.subtract(const Duration(days: 30)))) return const Icon(Icons.assignment);
    if (now.isBefore(dueDate.subtract(const Duration(days: 7)))) return const Icon(Icons.warning_amber_rounded);
    if (now.isBefore(dueDate.subtract(const Duration(days:3)))) return const Icon(Icons.warning_rounded);
    if (now.isBefore(dueDate)) return const Icon(Icons.error_outline);
    return const Icon(Icons.error);
  }
}