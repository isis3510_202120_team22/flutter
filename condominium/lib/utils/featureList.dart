// ignore_for_file: file_names

import 'package:flutter/material.dart';
import '../models/feature.dart';
import './compact_feature.dart';


// ignore: must_be_immutable
class FeatureList extends StatelessWidget {
  List<Feature> features;

  // ignore: use_key_in_widget_constructors
  FeatureList(this.features);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
      physics: const NeverScrollableScrollPhysics(),
      itemCount: features.length,
      itemBuilder: (context, index) => CompactFeature(features[index]),
    );
  }
}