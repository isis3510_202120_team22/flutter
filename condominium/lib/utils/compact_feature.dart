import 'package:condominium/models/feature.dart';
import 'package:condominium/payments/payments_view.dart';
import 'package:condominium/visitors/visitors_view.dart';
import 'package:flutter/material.dart';


// ignore: must_be_immutable
class CompactFeature extends StatelessWidget {
  Feature feature;

  CompactFeature(this.feature);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
      child: Container(
        decoration: _boxDecoration(context),
        height: 100.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
              mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  _infoJobTexts(context),
                  const SizedBox(height: 10.0 ),
                  TextButton(
                    onPressed: () {
                      //Navigator.push(context, MaterialPageRoute(builder: (context) => const Payments()));
                      if (feature.feat=="2") {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Visitors()));
                      }
                      else if (feature.feat =="3"){
                        Navigator.push(context, MaterialPageRoute(builder: (context) => const Payments()));
                      }
                    },
                    style: ButtonStyle(
                      backgroundColor:MaterialStateProperty.all<Color>(Theme.of(context).backgroundColor) ,
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Theme.of(context).backgroundColor)
                        )
                      )
                    ),
                  child: const Text("Go"))
                ],
              ),
              _companyLogo(),
          ],
        ),
      ),
    );
  }

  BoxDecoration _boxDecoration(context) {
    return BoxDecoration(
      color: Theme.of(context).cardColor,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }

  Widget _companyLogo() {
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Image.asset(feature.img),
    );
  }

  Widget _infoJobTexts(context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          feature.description,
          style: Theme.of(context).textTheme.headline3,
        ),
      ],
    );
  }

}