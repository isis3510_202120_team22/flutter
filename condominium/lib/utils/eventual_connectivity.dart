import 'dart:async';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EventualConnectivity extends StatefulWidget {
  const EventualConnectivity({Key? key, required this.updateConnectionStatus}) : super(key: key);

  final Function(ConnectivityResult) updateConnectionStatus;

  @override
  _EventualConnectivityState createState() => _EventualConnectivityState();
}

class _EventualConnectivityState extends State<EventualConnectivity> {

  ConnectivityResult _online = ConnectivityResult.none;

  final Connectivity _connectivity = Connectivity();

  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  @override
  void initState() {
    super.initState();
    initConnectivity();
    _connectivitySubscription =
      _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initConnectivity() async {
    late ConnectivityResult result;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
      return;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return Future.value(null);
    }

    return _updateConnectionStatus(result);
  }

  @override
  Widget build(BuildContext context) {
    return _online != ConnectivityResult.none? Container() :
        Container(
          margin: const EdgeInsets.symmetric(horizontal:15),
          decoration: _boxDecoration(context),
          alignment: Alignment.center,
          child: const Text(
          "No internet connection",
          style: TextStyle(color: Colors.black),
          ),
        );
      
      
  }

  BoxDecoration _boxDecoration(context) {
    return BoxDecoration(
      color: Theme.of(context).cardColor,
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black26,
          offset: Offset(2.0, 2.0),
          blurRadius: 8.0,
        ),
      ],
    );
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    widget.updateConnectionStatus(result);
    setState(() {
      _online = result;
    });
  }

  @override
  dispose() {
    super.dispose();

    _connectivitySubscription.cancel();
  }
}