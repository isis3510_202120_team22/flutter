import 'package:condominium/home/home.dart';
import 'package:condominium/visitors/visitors_view.dart';
import 'package:flutter/material.dart';
import 'package:condominium/payments/payments_view.dart';

Widget tittle(context){ 
    return RichText(
      textAlign:TextAlign.center,
      text: TextSpan(
        text: 'Condo',
        style: Theme.of(context).textTheme.headline1,
        children: const <TextSpan>[
          TextSpan(text: 'minium', style: TextStyle(fontWeight: FontWeight.bold, color: Color(0xFF92A3FD)) ),
        ],
      ),
    );
  }

Widget bottomBar(context){
  int _selectedIndex = 0;

  void _OnItemTap(int index){
    if (index==0){
        Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()));
    }
    if (index==1 && index!=_selectedIndex){
        Navigator.push(context, MaterialPageRoute(builder: (context) => const Visitors()));
    }
    if (index==3 && index!=_selectedIndex){
        Navigator.push(context, MaterialPageRoute(builder: (context) => const Payments()));
    }
    _selectedIndex=index;
  }

  return BottomNavigationBar(
    items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.groups),
            label: 'Visitors',
            backgroundColor: Colors.white
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.today),
            label: 'Booking',
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.receipt),
            label: 'Payments',
            backgroundColor: Colors.white,
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.account_circle),
            label: 'Profile',
            backgroundColor: Colors.white,
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: const Color(0xFF92A3FD), 
        onTap: _OnItemTap,
        );

}
