// ignore_for_file: must_be_immutable, use_key_in_widget_constructors

import 'package:condominium/home/home_view_model.dart';
import 'package:condominium/models/feature.dart';
import 'package:condominium/payments/payments_view_model.dart';
import 'package:condominium/utils/featureList.dart';
import 'package:condominium/utils/widgets.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';


class HomeScreen extends StatelessWidget {

  List<Feature> features = [
    Feature('        Book easily your \n         condo’s facilities','assets/img/booking.png', "1"),
    Feature('   Manage your visitors', 'assets/img/visitors.png', "2"),
    Feature('   Manage your payments', 'assets/img/payments.png', "3")
  ];

  late Future<bool> nearPayments;

  PaymentsViewModel paymentsViewModel = PaymentsViewModel();

  HomeViewModel homeViewModel = HomeViewModel();

  void informAccess() async {
    late ConnectivityResult result;
    try {
      result = await Connectivity().checkConnectivity();
    } on PlatformException catch (e) {
      print(e.toString());
    }
    homeViewModel.informAccess(result);
  }

  @override
  Widget build(BuildContext context) {
    informAccess();
     return Scaffold(
       backgroundColor: Theme.of(context).backgroundColor,
       
       body: SafeArea(
         child: ListView(
           children: <Widget>[
             tittle(context),
             _asyncList(),
             _features(),
           ],
         ),
       ),
       bottomNavigationBar: bottomBar(context),
     );
  }

  Widget _features(){
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: FeatureList(features),
        ),
      ],
    );
  }

  Widget _asyncList() {
    return FutureBuilder(
      future: paymentsViewModel.getNearPayments(),
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return Container(
            padding: const EdgeInsets.all(10.0),
            child: _notificationCard(snapshot.data , context)
          );
        } else if (snapshot.hasError) {
          return  Container(
            padding: const EdgeInsets.all(15.0),
            child: _notificationCard(snapshot.data , context)
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      }
    );
  }

  Widget _notificationCard(noti, context){
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 7.0),
      child: Container(
        decoration: _decoration(),
        height: 100.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _featureImage('assets/img/clock.png'),
            const SizedBox(width: 10),
            _infoText(context),
          ],
        ),
      ),
    );
  }


  BoxDecoration _decoration(){
    return BoxDecoration(
      color: const Color(0xFFC58BF2),
      borderRadius: BorderRadius.circular(10.0),
      boxShadow: const <BoxShadow> [
        BoxShadow(
          color: Colors.black45,
          offset: Offset(4.0, 4.0), 
          blurRadius: 10.0
        ),
      ]
    );
  }


  Widget _featureImage(route) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10.0),
        child: Image.asset(
          route,
          width: 80.0,
        ),
      ),
    );
  }

  Widget _infoText(context){
     return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(
          "Reminder!",
          style: Theme.of(context).textTheme.subtitle1,
        ),
        Text(
          "You have payments that \n are due in less than 3 days",
          style: Theme.of(context).textTheme.headline3,
        ),
     ]
     );
  }

}