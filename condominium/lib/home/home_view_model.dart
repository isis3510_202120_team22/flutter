import 'package:condominium/analytics/analytics.dart';
import 'package:condominium/models/user/user_singleton.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class HomeViewModel {
  User user = UserSingleton().currentUser;

  Analytics analytics = Analytics();

  void informAccess(ConnectivityResult online) {
    bool isOnline = !(online == ConnectivityResult.none);
    analytics.onlineTrace(user.token, isOnline, ViewType.home);
  }
}