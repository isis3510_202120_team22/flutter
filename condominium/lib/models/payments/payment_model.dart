import 'dart:convert';
import 'dart:io';

import 'package:condominium/models/cache/condo_cache_manager.dart';
import 'package:condominium/models/payments/payment.dart';
import 'package:http/http.dart' as http;

class PaymentModel {

  final paymentsUrl = "https://fathomless-everglades-41121.herokuapp.com/payments/";

  final nextPaymentsUrl = "https://fathomless-everglades-41121.herokuapp.com/near_unpaid_payments/";

  final monthPaid = "https://fathomless-everglades-41121.herokuapp.com/payments_ml_model/";

  final paymentsCache = 'payments';

  Future<List<Payment>> getPayments(String token) async {
    try {
      final response = await http.get(Uri.parse(paymentsUrl + token));
      if (response.statusCode == 200) {
        return responseToPaymentsList(response);
      } else {
        return Future.error("Unknown error");
      }
    } on SocketException {
      return Future.error("No internet connection");
    }
  }

  Future<List<Payment>> getPaymentsFromCache(String token) async {
    var response = await CondoCacheManager().getData(paymentsCache);
    if (response.statusCode == 200) {
      return responseToPaymentsList(response);
    }
    return Future(() => List<Payment>.empty());
  }

  Future<void> savePayments(List<Payment> payments) async{
    CondoCacheManager().saveData(paymentsUrl, payments, paymentsCache);
  }

  List<Payment> responseToPaymentsList(http.Response response) {
    List<Payment> payments = [];
    String body = utf8.decode(response.bodyBytes);
    final jsonData = jsonDecode(body);

    for (var jsonPayment in jsonData) {
      payments.add(Payment.fromJson(jsonPayment));
    }

    return payments;
  }

  Future<bool> getNextPayments(String token) async{
    try {
      final response = await http.get(Uri.parse(nextPaymentsUrl + token));
      List<Payment> payments = [];

      if (response.statusCode == 200) {
        String body = utf8.decode(response.bodyBytes);
        final jsonData = jsonDecode(body);

        for (var jsonPayment in jsonData) {
          payments.add(Payment.fromJson(jsonPayment));
        }

        return payments.isEmpty;
      } else {
        return Future.error("error");
      }
    } on SocketException {
      return Future.error("No internet connection");
    }
  }

  Future<double> getMonthPaid(String token) async{
    try {
      final month = DateTime.now().month.toString();
      final response = await http.get(Uri.parse(monthPaid + token + "/" + month));
      
      if (response.statusCode == 200) {
        String body = utf8.decode(response.bodyBytes);
        final jsonData = await jsonDecode(body);
        return double.parse(jsonData["prediction"]);
      } else {
        return Future.error("error");
      }
    } on SocketException {
      return Future.error("No internet connection");
    } 
  }
}