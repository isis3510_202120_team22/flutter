import 'package:flutter/foundation.dart';

class Payment {
  final int paymentId;
  final int residenceId;
  final DateTime startDate;
  final DateTime dueDate;
  final PaymentStatus status;
  final String concept;
  final double amount;
  final DateTime paymentDate;

  Payment(this.paymentId, this.residenceId, this.startDate, this.dueDate, this.status, this.concept, this.amount, this.paymentDate);

  factory Payment.fromJson(Map<String, dynamic> json) {
    final startDate = DateTime.parse(json['start_date']);
    final dueDate = DateTime.parse(json['due_date']);
    DateTime paymentDate = json['payment_date'] != null ? DateTime.parse(json['payment_date']) : DateTime.now();

    final jsonStatus = (json['status'] as String).toLowerCase();
    PaymentStatus status = PaymentStatus.overdue;
    if (jsonStatus == PaymentStatus.paid.name) {
      status = PaymentStatus.paid;
    } else if (jsonStatus == PaymentStatus.pending.name) {
      status = PaymentStatus.pending;
    }

    return Payment(json['payment_id'], json['residence_id'], startDate, dueDate, status, json['concept'], json['amount'], paymentDate);
  }

  Map toJson() => {
    "payment_id":paymentId,
    "residence_id":residenceId,
    "start_date":startDate.toString(),
    "due_date":dueDate.toString(),
    "status":status.name,
    "concept":concept,
    "amount":amount,
    "payment_date":paymentDate.toString()
  };
}

enum PaymentStatus {
  paid,
  pending,
  overdue
}

extension PaymentStatusExtension on PaymentStatus {
  String get name => describeEnum(this);
}