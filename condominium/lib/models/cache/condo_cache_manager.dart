import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:http/http.dart';

class CondoCacheManager {

  Future<Response> getData(String key) async {
    final fileInfo = await DefaultCacheManager().getFileFromCache(key);
    if (fileInfo != null) {
      final file = fileInfo.file;
      if (await file.exists()){
        final res = await file.readAsString();
        return Response(res, 200);
      }
    }
    return Response("No", 404);
  }

  Future<void> saveData(String url, Object data, String key) async {
    final string = jsonEncode(data);
    final bytes = utf8.encode(string);
    final bytes2 = Uint8List.fromList(bytes);
    DefaultCacheManager().putFile(url, bytes2, key: key);
  }

  void clearCache() {
    DefaultCacheManager().emptyCache();
  }
}