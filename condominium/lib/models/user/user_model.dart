import 'package:condominium/models/user/user_singleton.dart';

class UserModel {
  Future<User> getUser() {
    return Future.delayed(const Duration(seconds:  2), () => User("a token"));
  }
}