import 'package:flutter/material.dart';

class User {
  final String token;

  User(this.token);
}

abstract class UserBase {
  @protected
  late User initialUser;
  @protected
  late User user;

  User get currentUser => user;

  void setUser(User user) {
    this.user = user;
  }

  void reset() {
    user = initialUser;
  }
}

class UserSingleton extends UserBase {
  static final UserSingleton _instance = UserSingleton._internal();

  factory UserSingleton() {
    return _instance;
  }

  UserSingleton._internal() {
    initialUser = User("invalid token");
    user = initialUser;
  }
}