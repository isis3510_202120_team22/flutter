class Visitor {
  final String visitorId;
  final String firstName;
  final String lastName;
  final int telephone;
  final String documentType;

  Visitor(this.visitorId, this.firstName, this.lastName, this.telephone, this.documentType);

  factory Visitor.fromJson(Map<String, dynamic> json) {
    return Visitor(json['document_id'], json['first_name'], json['second_name'], json['phone'], json['document_type']);
  }

  Map toJson() => {
    'document_id': visitorId,
    'first_name': firstName,
    'second_name': lastName,
    'phone': telephone,
    'document_type': documentType
  };
}