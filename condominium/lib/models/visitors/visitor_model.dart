import 'dart:convert';
import 'dart:io';

import 'package:condominium/models/cache/condo_cache_manager.dart';
import 'package:condominium/models/visitors/visitor.dart';
import 'package:http/http.dart' as http;

class VisitorModel {

  final visitorsUrl = "https://fathomless-everglades-41121.herokuapp.com/visitors/nofrequent/";
  final frequentUrl = "https://fathomless-everglades-41121.herokuapp.com/visitors/frequent/";
  final nonFrequentUrl = "https://fathomless-everglades-41121.herokuapp.com/visitors/last3months/";
  final visitorsCache = 'visitors';
  final frequentCache = 'frequent';


  Future<List<Visitor>> getNonFrequentVisitors(String token) async {
    try {
      final response = await http.get(Uri.parse(nonFrequentUrl + token));

      if (response.statusCode == 200) {
        return responseToVisitorsList(response, false);
      } else {
        return Future.error("Unknown error");
      }
    } on SocketException {
      return Future.error("No internet connection");
    }
  }

  Future<List<Visitor>> getVisitors(String token) async {
    try {
      final response = await http.get(Uri.parse(visitorsUrl + token));

      if (response.statusCode == 200) {
        return responseToVisitorsList(response, false);
      } else {
        return Future.error("Unknown error");
      }
    } on SocketException {
      return Future.error("No internet connection");
    }
  }

  Future<List<Visitor>> getVisitorsFromCache(String token) async {
    var response = await CondoCacheManager().getData(visitorsCache);
    if (response.statusCode == 200) {
      return responseToVisitorsList(response, true);
    }
    return Future.delayed(const Duration(seconds: 3), () => List<Visitor>.empty());
  }

  Future<void> saveVisitors(String token, List<Visitor> visitors) async {
    CondoCacheManager().saveData(visitorsUrl + token, visitors, visitorsCache);
  }

  Future<List<Visitor>> getFrequentVisitors(String token) async {
    try {
      final response = await http.get(Uri.parse(frequentUrl + token));
      if (response.statusCode == 200) {
        return responseToVisitorsList(response, false);
      } else {
        return Future.error("Unknown error");
      }
    } on SocketException {
      return Future.error("No internet connection");
    }
  }

  Future<List<Visitor>> getFrequentVisitorsFromCache(String token) async {
    var response = await CondoCacheManager().getData(frequentCache);
    if (response.statusCode == 200) {
      return responseToVisitorsList(response, true);
    }
    return Future(() => List<Visitor>.empty());
  }

  Future<void> saveFrequentVisitors(String token, List<Visitor> visitors) async {
    CondoCacheManager().saveData(frequentUrl + token, visitors, frequentCache);
  }

  List<Visitor> responseToVisitorsList(http.Response response, bool fromCache) {
    List<Visitor> visitors = [];
    String body = fromCache? const Latin1Decoder().convert(response.bodyBytes): utf8.decode(response.bodyBytes);
    final jsonData = jsonDecode(body);

    for (var jsonVisitor in jsonData) {
      visitors.add(Visitor.fromJson(jsonVisitor));
    }

    return visitors;
  }
}