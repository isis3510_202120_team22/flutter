import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class Analytics {

  final cacheUrl = "https://fathomless-everglades-41121.herokuapp.com/cacheamount/";
  final onlineUrl = "https://fathomless-everglades-41121.herokuapp.com/online/";

  void cachedData(String token, int number) async {
    try{
      await http.get(Uri.parse(cacheUrl + token + "/ios"  + number.toString()));
    } on SocketException{
      print("Socket error sending info");
    }
  }

  void onlineTrace(String token, bool online, ViewType view) async{
    String value = (online? 1 : 0).toString();
    try {
      await http.get(Uri.parse(onlineUrl + token + "/ios" + value + "/" + view.name));
    } on SocketException{
      print("Socket error sending info");
    }
  }
}

enum ViewType {
  home, 
  payments,
  visitors,
  visits,
  add_visitor
}

extension ViewTypeExtension on ViewType {
  String get name => describeEnum(this);
}